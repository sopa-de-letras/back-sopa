import { Injectable } from '@nestjs/common';
import { SopaDTO } from './dto/sopa.dto';

@Injectable()
export class AppService {
  getSopa(sopa: SopaDTO, ren_max: number, col_max: number) {
    const sopaLetras: string[][] = [];

    let index = 0;
    for (const palabra of sopa.palabraSopaForm) {
      sopa.palabraSopaForm[index] = palabra.toLocaleUpperCase();
      index++;
    }

    for (let ren = 0; ren < ren_max; ren++) {
      const columna: string[] = [];
      for (let col = 0; col < col_max; col++) {
        columna.push(sopa.sopaForm[col + ren * col_max].toUpperCase());
      }
      sopaLetras.push(columna);
    }
    return this.busca(sopa.palabraSopaForm, sopaLetras, ren_max, col_max);
  }

  busca(ciertas: string[], sopa: string[][], ren_max: number, col_max: number) {
    const palabrasEncontradas: any[] = [];
    for (let palabra = 0; palabra < ciertas.length; palabra++) {
      //palabra = tiburon
      for (let ren = 0; ren < ren_max; ren++) {
        for (let col = 0; col < col_max; col++) {
          if (sopa[ren][col] === ciertas[palabra][0]) {
            //encontre la primer letra
            const coor: any[] = [];
            coor[0] = [
              {
                letra: ciertas[palabra][0],
                ren: ren,
                col: col,
                ren_ant: ren,
                col_ant: col,
              },
            ];
            let i = coor[0].length;
            for (let letra = 1; letra < ciertas[palabra].length; letra++) {
              let a: any[] = [];
              for (let j = 0; j < i; j++) {
                const encontro: any = this.encuentraLetraColindante(
                  coor[letra - 1][j].ren,
                  coor[letra - 1][j].col,
                  ciertas[palabra][letra],
                  sopa,
                  ren_max,
                  col_max,
                  coor[letra - 1][j].ren_ant,
                  coor[letra - 1][j].col_ant,
                );
                a = a.concat(encontro);
              }
              coor[letra] = a;
              i = coor[letra].length;
              let continua = false;
              for (let q = 0; q < coor[letra].length; q++) {
                let continuaOp = false;
                if (typeof coor[letra][q].letra === 'boolean') {
                  continuaOp = coor[letra][q].letra;
                } else {
                  continuaOp = true;
                }
                continua = continua || continuaOp;
              }
              if (!continua) {
                break;
              }
              if (letra == ciertas[palabra].length - 1) {
                let infectado = false;
                let desinfectado = false;
                let coor_infectado = [];
                for (let ren_coor = coor.length - 1; ren_coor > 0; ren_coor--) {
                  for (
                    let col_coor = 0;
                    col_coor < coor[ren_coor].length;
                    col_coor++
                  ) {
                    if (coor[ren_coor][col_coor].letra == false) {
                      if (desinfectado) {
                        infectado = false;
                      } else {
                        infectado = true;
                      }
                      coor_infectado.push(coor[ren_coor][col_coor]);
                    }
                  }
                  if (infectado) {
                    for (
                      let i_coor_infectado = 0;
                      i_coor_infectado < coor_infectado.length;
                      i_coor_infectado++
                    ) {
                      for (
                        let col_coor = 0;
                        col_coor < coor[ren_coor - 1].length;
                        col_coor++
                      ) {
                        if (
                          coor[ren_coor - 1][col_coor].ren ==
                          coor_infectado[i_coor_infectado].ren_ant
                        ) {
                          if (
                            coor[ren_coor - 1][col_coor].col ==
                            coor_infectado[i_coor_infectado].col_ant
                          ) {
                            if (coor[ren_coor - 1].length > 1) {
                              if (!desinfectado) {
                                coor[ren_coor - 1][col_coor].letra = false;
                                for (
                                  let t = 0;
                                  t < coor[ren_coor - 1].length;
                                  t++
                                ) {
                                  if (
                                    coor[ren_coor - 1][t].ren ==
                                    coor[ren_coor - 1][col_coor].ren
                                  ) {
                                    if (
                                      coor[ren_coor - 1][t].col ==
                                      coor[ren_coor - 1][col_coor].col
                                    ) {
                                      coor[ren_coor - 1][t].letra = false;
                                    }
                                  }
                                }

                                let iterador = 1;
                                for (
                                  let ite = coor[ren_coor - 1].length - 1;
                                  ite > 0;
                                  ite--
                                ) {
                                  if (
                                    coor[ren_coor - 1][ite].col_ant ==
                                    coor[ren_coor - 1][ite - 1].col_ant
                                  ) {
                                    if (
                                      coor[ren_coor - 1][ite].ren_ant ==
                                      coor[ren_coor - 1][ite - 1].ren_ant
                                    ) {
                                      iterador++;
                                    }
                                  }
                                }
                                if (iterador == coor[ren_coor - 1].length) {
                                  desinfectado = true;
                                }
                              }
                            }
                          }
                        }
                      }
                    }
                    coor_infectado = [];
                  }
                }
                palabrasEncontradas.push({
                  palabra: ciertas[palabra],
                  coor: coor,
                });
              }
            }
          }
        }
      }
    }
    return palabrasEncontradas;
  }

  encuentraLetraColindante(
    ren: number,
    col: number,
    letra: string,
    sopa: string[][],
    ren_max: number,
    col_max: number,
    ren_ant: number,
    col_ant: number,
  ) {
    const encontro: any[] = [];
    let col_b = col;
    col_b--;
    //3 nivles de comparacion, arriba,abajo, en su mismo eje
    for (let j = 0; j < 3; j++) {
      //col
      let ren_b = ren;
      ren_b--;
      for (let i = 0; i < 3; i++) {
        //ren
        //compara si esta dentro de un rango que se pueda buscar
        if (ren_b >= 0 && col_b >= 0) {
          if (ren_b < ren_max && col_b < col_max) {
            if (sopa[ren_b][col_b] === letra) {
              encontro.push({
                letra: letra,
                ren: ren_b,
                col: col_b,
                ren_ant: ren,
                col_ant: col,
              });
              if (ren_b === ren_ant) {
                if (col_b === col_ant) {
                  encontro.pop();
                }
              }
            }
          }
        }
        ren_b++;
      }
      col_b++;
    }
    if (encontro.length == 0) {
      encontro.push({
        letra: false,
        ren: false,
        col: false,
        ren_ant: ren,
        col_ant: col,
      });
    }

    return encontro;
  }
}
