import { Body, Controller, Get, Param, Post } from '@nestjs/common';
import { AppService } from './app.service';
import { SopaDTO } from './dto/sopa.dto';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Post('/sopa/:ren/:col')
  getSopa(@Body() sopa: SopaDTO, @Param('ren') ren, @Param('col') col) {
    return this.appService.getSopa(sopa, ren, col);
  }
}
