export interface Encontro {
  letra: string | boolean;
  ren: number | boolean;
  col: number | boolean;
  ren_ant: number | boolean;
  col_ant: number | boolean;
}

export interface PalabrasEncontradas {
  palabras: string;
  coor: Encontro;
}
